<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/record-transfer','JsonImportController@index');
Route::get('/define-callback-js','CallbackController@index');
Route::get('/second-buyer-eloquent','BuyerController@eloquent');
Route::get('/second-buyer-no-eloquent','BuyerController@withouteloquent');
Route::get('/purchase-list-eloquent','PurchaseController@eloquent');
Route::get('/purchase-list-no-eloquent','PurchaseController@withouteloquent');
Route::get('/sort-js','JavascriptController@sort');
Route::get('/filter-js','JavascriptController@filter');
Route::get('/foreach-js','JavascriptController@foreachjs');
Route::get('/map-js','JavascriptController@map');
Route::get('/reduce-js','JavascriptController@reduce');
Route::get('/i-m-funny','AnswerController@answer');
