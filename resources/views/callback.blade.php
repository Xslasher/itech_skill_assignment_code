<!DOCTYPE html>
<html>
<h1>Callback function code</h1>

<body>
<script type="text/javascript">
    var data = {email:'trump@gmail.com', age:70};
    function age(data) {
        if(data.age < 18){
            console.log('Email is valid');
        }
        else{
            console.log('Email is inValid');
        }
    }
    function checkAge(callback) {
        callback(data);
    }
    checkAge(age);

</script>
</body>
</html>



