<!DOCTYPE html>
<html>
<h1>Sort function code</h1>
<button onclick="sort()">Sort</button>
<body>
<script type="text/javascript">
    var data = [0, 5 , 3, 9, 100, 25, 1];
    function sort() {
        var result=data.sort(function (a,b) {
            if(a>b) return 1;
            if(a<b) return -1;
            return 0;

        });
        console.log(result);
    }

</script>
</body>
</html>
