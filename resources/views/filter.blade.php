<!DOCTYPE html>
<html>
<h1>Filter function code</h1>
<button onclick="age()">Filter</button>
<body>
<script type="text/javascript">
    var ages = [32, 33, 16, 40];
    function checkAge(age) {
        return age>=18;
    }
    function age() {
        var result=ages.filter(checkAge);
        console.log(result);
    }

</script>
</body>
</html>
