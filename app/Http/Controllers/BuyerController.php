<?php

namespace App\Http\Controllers;

use App\Buyer;
use Illuminate\Support\Facades\DB;

class BuyerController extends Controller{
    public function eloquent(){
        $buyer=Buyer::orderBy('total_items_taken','DESC')->skip(1)->first();
        dd($buyer);
    }

    public function withouteloquent(){
        $buyer=DB::table('buyers')->orderBy('total_items_taken','DESC')->skip(1)->first();
        dd($buyer);
    }
}
