<?php

namespace App\Http\Controllers;
use App\Record;
use Illuminate\Support\Facades\DB;

class JsonImportController extends Controller{
    public function index(){
        $json=file_get_contents(public_path('storage/records.json'));
        $data=json_decode($json,true);
        $collection=collect($data['RECORDS']);
        $chunks=$collection->chunk(500);
        foreach ($chunks as $chunk){
            Record::insert($chunk->toArray());
        }
        return 'Data Inserted';
    }
}
