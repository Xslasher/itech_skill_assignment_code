<?php

namespace App\Http\Controllers;

class JavascriptController extends Controller{
    public function sort(){
        return view('sort');
    }

    public function filter(){
        return view('filter');
    }

    public function map(){
        return view('map');
    }

    public function reduce(){
        return view('reduce');
    }
}
