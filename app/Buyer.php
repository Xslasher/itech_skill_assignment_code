<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table='buyers';
    protected $fillable=['buyer_name','total_dairy_taken','total_pen_taken','total_eraser_taken','total_items_taken'];
}
